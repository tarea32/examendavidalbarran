﻿using System;

namespace ExamenC_
{
    internal class Calculadora
    {
        private int num1;
        private int num2;
        public Calculadora(int num1, int num2)
        {
            this.num1 = num1;
            this.num2 = num2;
        }

        public void sumar()
        {
            Console.WriteLine("La suma es: "+(num1+num2));
        }

        public void restar()
        {
            Console.WriteLine("La resta es: "+(num1-num2));
        }

        public void multiplicar()
        {
            Console.WriteLine("La multiplicación es: "+(num1*num2));
        }

        //Esta clase es la única capaz de generar excepciones como dividir por 0 que se ha controlado correctamente
        public void dividir()
        {
            try
            {
                Console.WriteLine("La división es: " + (num1 / num2));
            }catch (Exception)
            {
                Console.WriteLine("No se puede divir por 0");
            }
            

        }
    }
}
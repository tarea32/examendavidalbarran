﻿using System;
using System.ComponentModel.Design;
using System.Linq;

namespace ExamenC_
{
    internal class Programa
    {
        public Programa()
        {
        }

        internal void ejecutar()
        {
            menu();
        }

        private void menu()
        {
            Console.WriteLine("1.Manipulación de Arrays\n"+
                                "2.Clases y Herencia\n" +
                                "3.Manejo de Excepciones Avanzado\n" +
                                "4.Manipulación de Cadenas\n" +
                                "5.Manipulación de Cadenas y Expresiones Regulares\n");

            Console.WriteLine("\nIntroduzca una opción:");
            string opcion=Console.ReadLine();
            ejecutarOpcion(opcion);
        }

        private void ejecutarOpcion(string opcion)
        {
            switch (opcion)
            {
                case "1":
                    ejercicio1();
                    break;
                case "2":
                    ejercicio2();
                    break;
                case "3":
                    ejercicio3();
                    break;
                case "4":
                    ejercicio4();
                    break;
                case "5":
                    ejercicio5();
                    break;
                default:
                    Console.WriteLine("Opción inválida");
                    break;
            }
        }

        private void ejercicio5()
        {
            Console.WriteLine("Introduce un texto: ");
            string texto = Console.ReadLine();

            if (comprobarPalindromo(texto))
            {
                Console.WriteLine("Es palíndromo");
            }
            else
            {
                Console.WriteLine("No es palíndromo");
            }
        }

        private bool comprobarPalindromo(string texto)
        {
            string textoUsar = texto.Trim().ToLowerInvariant();
            string textoInvertido="";

            for (int i = textoUsar.Length; i > 0; i--)
            {
                textoInvertido += textoUsar[i-1];
            }

            Console.WriteLine(textoUsar);
            Console.WriteLine(textoInvertido);

            if (textoUsar.Equals(textoInvertido))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ejercicio4()
        {
            Console.WriteLine("Introduce una cadena: ");
            string cadena=Console.ReadLine();
            Console.WriteLine(invertirPalabrasCadena(cadena));
        }

        private string invertirPalabrasCadena(string cadena)
        {
            string[] arrayPalabras=cadena.Split(' ');

            string cadenaInvertida="";
            for (int i = arrayPalabras.Length; i > 0; i--)
            {
                cadenaInvertida+= arrayPalabras[i-1]+" ";
            }

            return cadenaInvertida;
        }

        private void ejercicio3()
        {
            try
            {
                Console.WriteLine("Introduce el número 1: ");
                int num1 = int.Parse(Console.ReadLine());
                Console.WriteLine("Introduce el número 2: ");
                int num2 = int.Parse(Console.ReadLine());
                Calculadora calculadora = new Calculadora(num1, num2);

                calculadora.sumar();
                calculadora.restar();
                calculadora.multiplicar();
                calculadora.dividir();

            }catch(ArgumentNullException ane) {
                Console.WriteLine("Error.No se ha introducido nada.");
            }catch (FormatException fe){
                Console.WriteLine("Error.No se puede pasar a entero.");
            }catch(Exception ex) {
                Console.WriteLine("Error inesperado");
            }
        }

        private void ejercicio2()
        {
            Console.WriteLine("Introduce la marca: ");
            string marca=Console.ReadLine();
            Console.WriteLine("Introduce el modelo: ");
            string modelo = Console.ReadLine();
            Console.WriteLine("Introduce el año: ");
            string anio = Console.ReadLine();

            Vehiculo vehiculo = new Vehiculo(marca,modelo);
            Console.WriteLine("Clase Vehiculo(Padre): ");
            vehiculo.Informacion();

            Automovil automovil = new Automovil(marca,modelo,anio);
            Console.WriteLine("Clase Automovil(Hijo): ");
            automovil.InformacionAutomovil();
        }

        private void ejercicio1()
        {
            int[] enteros=new int[8];

            for (int i = 0;i<enteros.Length;i++)
            {
                enteros[i]=i+1;
            }
            Console.WriteLine(sumarArrayEnteros(enteros));
        }

        private int sumarArrayEnteros(int[] enteros)
        {
            int suma=0;

            foreach (int i in enteros) {
                suma += i;
            }

            return suma;
        }
    }
}
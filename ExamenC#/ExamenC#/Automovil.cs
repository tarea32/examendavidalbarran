﻿using System;

namespace ExamenC_
{
    internal class Automovil : Vehiculo
    {
        private string anio;
        public Automovil(string marca, string modelo, string anio) : base(marca, modelo)
        {
            this.anio = anio;
        }

        public void InformacionAutomovil()
        {
            base.Informacion();
            Console.WriteLine("Año: "+anio);
        }
    }
}
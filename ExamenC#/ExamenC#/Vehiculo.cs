﻿using System;

namespace ExamenC_
{
    internal class Vehiculo
    {
        private string marca;
        private string modelo;

        public Vehiculo(string marca, string modelo)
        {
            this.marca = marca;
            this.modelo = modelo;
        }

        public void Informacion()
        {
            Console.WriteLine("Marca: "+marca+"\nModelo: "+modelo);
        }
    }
}
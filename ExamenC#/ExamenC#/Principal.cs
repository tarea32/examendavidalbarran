﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenC_
{
    internal class Principal
    {
        static void Main(string[] args)
        {
            try
            {
                Programa programa = new Programa();
                programa.ejecutar();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message+"\n"+ex.StackTrace);
            }
        }
    }
}
